//
//  UIImageView+Extension.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 12.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import Foundation
import UIKit
import Parse

extension UIImageView {
    
//    func imageFromPFFile(file: PFFile?) {
//        file?.getDataInBackground({ (data, error) in
//            if let data = data, let image = UIImage(data: data) {
//                DispatchQueue.main.async(execute: { () -> Void in
//                    self.image = image
//                    UIView.animate(withDuration: 0.3, animations: { () -> Void in
//                        self.alpha = 1
//                    }, completion: { (finished: Bool) -> Void in
//                    })
//                })
//            }
//        })
//    }
    
    func imageFromPFFile(file: PFFile?) {
        file?.getDataInBackground({ (data, error) in
            if let data = data, let image = UIImage(data: data) {
                DispatchQueue.main.async {
                    self.image = image
                }
            }
        })
    }
}
