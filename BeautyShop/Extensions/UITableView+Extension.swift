//
//  File.swift
//  Caledonia
//
//  Created by Nikola Andriiev on 23.11.16.
//  Copyright © 2016 Andriiev.Mykola. All rights reserved.
//

import Foundation
import UIKit
import Parse

extension UITableView {
    
    // MARK: - Get selected items
    func selectedItems<T>(_ sourceItems: [T]) -> [T]? {
        if let selectedRows = self.indexPathsForSelectedRows {
            var items = [T]()
            for index in selectedRows {
                items.append(sourceItems[index.row])
            }
            return items
        }
        return nil
    }
}

extension UITableView  {
    //DESC: cell csl should macth to identifier
    func headerViewFromCell<T: UITableViewCell>(cls: T.Type) -> UIView {
        let identifier = String(describing: cls.self)
        let cell = self.dequeueReusableCell(withIdentifier: identifier)!
        
        let containerView = UIView(frame: cell.frame)
        cell.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        containerView.addSubview(cell)
        
        return containerView
    }
    
    // DESC: cls name should match with cell identifier
    func registerCell<T: UITableViewCell>(_ cls: T.Type) {
        let name = String(describing: cls.self)
        let cell = UINib.init(nibName: name, bundle: nil)
        self.register(cell, forCellReuseIdentifier: name)
    }
    
    func registerHeader(_ cls: AnyClass?) {
        if let cls = cls {
            let name = String(describing: cls.self)
            let nib = UINib.init(nibName: name, bundle: nil)
            self.register(nib, forHeaderFooterViewReuseIdentifier: name)
        }
    }
    
    func dequeueHeader<T>(cls: T.Type) -> T? {
        let clsString = String(describing: T.self)
        return self.dequeueReusableHeaderFooterView(withIdentifier: clsString) as? T
    }
    
    func dequeueCell<T>(cls: T.Type, indexPath path: IndexPath) -> T? {
        let clsString = String(describing: T.self)
        return self.dequeueReusableCell(withIdentifier: clsString, for: path) as? T
    }
    
    func update(_ block: (() -> ())? = nil) {
        self.beginUpdates()
        block?()
        self.endUpdates()
    }
    
    func rowAutoHeight(estimatedRowHeight : CGFloat) {
        self.rowHeight = UITableViewAutomaticDimension
        self.estimatedRowHeight = estimatedRowHeight
    }
    
    func reload() {
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
    
    func reloadRowsNoAnimationAt(paths: [IndexPath], animation: UITableViewRowAnimation = .none) {
        let offset = self.contentOffset
        UIView.setAnimationsEnabled(false)
        self.update({
            self.reloadRows(at: paths, with: animation)
        })
        
        self.setContentOffset(offset, animated: false)
        UIView.setAnimationsEnabled(true)
    }
    
    func insertRowNoAnimation(path: IndexPath) {
        UIView.performWithoutAnimation {
            let offset = self.contentOffset
            self.insertRows(at: [path], with: .none)
            self.setContentOffset(offset, animated: false)
            UIView.setAnimationsEnabled(true)
        }
    }
    
    func insertSectionAnimationAt(inx: Int) {
        let offset = self.contentOffset
        UIView.setAnimationsEnabled(false)
        self.update({
            self.insertSections(IndexSet(integer: inx), with: .none)
        })
        
        self.setContentOffset(offset, animated: false)
        UIView.setAnimationsEnabled(true)
    }
}

// MARK: - Handle Scroll
extension UITableView {
    func scrollToBottom(delay: Double, animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + delay, execute: { [weak self] in
            self?.scrollToBottom(animated: animated)
        })
    }
    
    func scrollToBottom(animated: Bool) {
        guard self.numberOfSections > 0 else { return }
        let lastSectionInx = self.numberOfSections - 1
        let countInLastSection = self.numberOfRows(inSection: lastSectionInx)
        let lastRowInx = countInLastSection > 0 ? countInLastSection - 1 : 0
        let path = IndexPath(row: lastRowInx, section: lastSectionInx)
        
        self.scrollToIndexPath(path, animated: animated)
    }
    
    func scrollToIndexPath(_ indexPath: IndexPath, animated: Bool) {
        if self.numberOfSections <= indexPath.section { return }
        let numberOfRows = self.numberOfRows(inSection: indexPath.section)
        if numberOfRows == 0 { return }
        
        let tableViewContentHeight = self.contentSize.height
        let isContentTooSmall = tableViewContentHeight < self.bounds.height
        
        if isContentTooSmall {
            self.scrollRectToVisible(CGRect(x: 0, y: tableViewContentHeight - 1, width: 1, height: 1), animated: animated)
            
            return
        }
        
        let row = max(min(indexPath.row, numberOfRows - 1), 0)
        let newIndexPath = IndexPath(row: row, section: indexPath.section)
        
        let cellHeight: CGFloat = self.delegate?.tableView!(self, heightForRowAt: newIndexPath) ?? 0
        let maxHeightForVisibleRow = self.bounds.height - self.contentInset.top - self.contentInset.bottom
        
        let scrollPosition: UITableViewScrollPosition = cellHeight > maxHeightForVisibleRow ? .bottom : .top
        
        self.scrollToRow(at: newIndexPath, at: scrollPosition, animated: animated)
    }
}
