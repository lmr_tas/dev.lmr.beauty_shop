//
//  PFGeoPoint+Extension.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 19.01.2018.
//  Copyright © 2018 Alexey Trotsenko. All rights reserved.
//

import UIKit
import Parse

extension PFGeoPoint {
    var coordinate: CLLocationCoordinate2D {
        return CLLocationCoordinate2D(latitude: self.latitude, longitude: self.longitude)
    }
}
