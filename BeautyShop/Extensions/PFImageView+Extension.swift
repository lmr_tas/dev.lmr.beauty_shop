//
//  PFImageView+Extension.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 07.02.2018.
//  Copyright © 2018 Alexey Trotsenko. All rights reserved.
//

import Foundation
import ParseUI

class CustomPFImageView: PFImageView {
     override var file: PFFile? {
        didSet {
            let indicator = UIActivityIndicatorView(activityIndicatorStyle: .gray)
            indicator.hidesWhenStopped = true
            indicator.frame.origin = CGPoint(x: self.frame.width/2 - indicator.frame.width/2,
                                             y: self.frame.height/2 - indicator.frame.height/2)
            self.addSubview(indicator)
            indicator.startAnimating()
            self.load { (image, error) in
            indicator.stopAnimating()
                self.image = image
            }
        }
    }
}
