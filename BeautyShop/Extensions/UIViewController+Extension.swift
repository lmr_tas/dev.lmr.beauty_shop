//
//  UIViewController+Extension.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 12.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit

extension UIViewController {
    
    
    
    class func currentOn(_ storyboard: String) -> UIViewController {
        let storyboard = UIStoryboard(name: storyboard, bundle: nil)
        let controllerName = String(describing:self)
        if let availableIdentifiers = storyboard.value(forKey: "identifierToNibNameMap") as? [String: Any],
            availableIdentifiers[controllerName] == nil {
            UIApplication.showAlertMessageWithAction("App ViewController NOT found", handler: {
                exit(-1)
            })
        }
        return storyboard.instantiateViewController(withIdentifier: controllerName)
    }
}

