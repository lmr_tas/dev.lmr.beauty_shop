//
//  UICollectionView+Extension.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 12.01.2018.
//  Copyright © 2018 Alexey Trotsenko. All rights reserved.
//

import UIKit

extension UICollectionView {
    
    //DESC: cell csl should macth to identifier
    func registerCell<T: UICollectionViewCell>(_ cls: T.Type) {
        let name = String(describing: cls.self)
        let cell = UINib.init(nibName: name, bundle: nil)
        self.register(cell, forCellWithReuseIdentifier: name)
    }
    
    func dequeueCell<T>(cls: T.Type, indexPath path: IndexPath) -> T? {
        let clsString = String(describing: T.self)
        return self.dequeueReusableCell(withReuseIdentifier: clsString, for: path) as? T
    }
    
    func reload() {
        DispatchQueue.main.async {
            self.reloadData()
        }
    }
}

//MARK: Scroll
extension UICollectionView {
    func scrollToTop(animated: Bool) {
        let indexPath = IndexPath(row: 0, section: 0)
        if self.cellForItem(at: indexPath) != nil {
            DispatchQueue.main.async {
                self.scrollToItem(at: indexPath, at: .top, animated: animated)
            }
        }
    }
}

