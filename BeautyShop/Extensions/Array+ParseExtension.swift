//
//  Array+ParseExtension.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 08.01.2018.
//  Copyright © 2018 Alexey Trotsenko. All rights reserved.
//

import Foundation
import Parse


extension Array {
    
    func indexOf<T: PFObject>(_ object: T?) -> Int? {
        if let objects = self as? [PFObject], let object = object {
            for (idx, obj) in objects.enumerated() {
                if obj.objectId == object.objectId {
                    return idx
                }
            }
        }
        return nil
    }
    
    
//    func removePFObject<T: PFObject>(_ object: T) -> Bool {
//        guard let array = self as? [PFObject] else {
//            return false
//        }
//
//        let obj = array.map{if $0.objectId == object.objectId {return $0.inde}}
//        print(obj)
//
//        return true
//    }
}
