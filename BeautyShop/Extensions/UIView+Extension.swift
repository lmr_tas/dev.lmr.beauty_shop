//
//  UIView+Extension.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 12.01.2018.
//  Copyright © 2018 Alexey Trotsenko. All rights reserved.
//

import UIKit

class RoundedView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
            layer.masksToBounds = cornerRadius > 0
        }
    }
}
