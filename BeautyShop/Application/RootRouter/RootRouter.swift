//  RootRouter.swift

import UIKit
import Parse
import SVProgressHUD
import IQKeyboardManagerSwift
import ParseFacebookUtilsV4

class RootRouter {
    
    var topViewController: UIViewController? {
        return UIApplication.topViewController()
    }
    
    /// window for navigation
    var window: UIWindow?
    
    // MARK: - Singleton
    static let shared = RootRouter()
    
    /// Custom didFinishLaunchingWithOptions method
    /// Tells the delegate that the launch process is almost done and the app is almost ready to run.
    ///
    /// - Parameters:
    ///   - launchOptions: A dictionary indicating the reason the app was launched
    ///   - window: The app's visible window
    /// - Returns: NO if the app cannot handle the URL resource or continue a user activity, otherwise return YES. The return value is ignored if the app is launched as a result of a remote notification.
    
    func application(didFinishLaunchingWithOptions launchOptions: [AnyHashable: Any]?, window: UIWindow) -> Bool {
        
        let parseConfig = ParseClientConfiguration {
            $0.applicationId    = "7nkKeXvnwvypuO7ArFsrupt5tibtLt0ff6YZ9PfU"
            $0.clientKey        = "0ZFiSGJhDJDd2qyN27ZGIhBX9JTCtbabEbTfvKrW"
            $0.server           = "https://parseapi.back4app.com"
            $0.isLocalDatastoreEnabled = true
        }
        Parse.initialize(with: parseConfig)
        PFFacebookUtils.initializeFacebook(applicationLaunchOptions: launchOptions)
        RootRouter.shared.window = window
        IQKeyboardManager.sharedManager().enable = true
        //Get&pin categories for fast query
        PFObject.unpinAllObjectsInBackground()
        ClientAPI.shared.downloadCategoriesForLocalUse()

        return true
    }
}


extension UIApplication: PFUserAuthenticationDelegate {
    public func restoreAuthentication(withAuthData authData: [String : String]?) -> Bool {
        print("[restoreAuthentication]",authData as Any)
        return true
    }
}


// MARK: - Additional extension for UIApplication
extension UIApplication {

    //MARK: Activity indicator
    class func showHUD(withMask: SVProgressHUDMaskType) {
        SVProgressHUD.setDefaultMaskType(withMask)
        DispatchQueue.main.async {
            SVProgressHUD.show()
        }
    }
    
    class func hideHUD() {
        DispatchQueue.main.async {
            SVProgressHUD.dismiss()
        }
    }
    
    //MARK: Get top ViewController
    class func topViewController(_ base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        
        if let navigationController = base as? UINavigationController {
            return topViewController(navigationController.visibleViewController)
        }
        
        if let tabBarController = base as? UITabBarController {
            if let selected = tabBarController.selectedViewController {
                return topViewController(selected)
            }
        }
        
        if let presentedViewController = base?.presentedViewController {
            return topViewController(presentedViewController)
        }
        
        if base == nil {
            return UIApplication.shared.delegate?.window??.rootViewController
        }
        return base
    }
    
    
    //MARK: - AlertView
    class func showAlertMessage(_ text: String) {
        print("[ALERT: \(text)]")
        let alert = UIAlertController(title: "Information", message: text, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.cancel, handler: nil))
        DispatchQueue.main.async {
            UIApplication.topViewController()!.present(alert, animated: true, completion: nil)
        }
    }
    
    class func showAlertMessageWithAction(_ text: String, handler: @escaping () -> ()) {
        print("[ALERT: \(text)]")
        let alert = UIAlertController(title: "Information", message: text, preferredStyle: UIAlertControllerStyle.alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (_) in
            handler()
        }))
        DispatchQueue.main.async {
            UIApplication.topViewController()!.present(alert, animated: true, completion: nil)
        }
    }
    
    class func showAlertWithTextField(title: String?, message: String?, fieldText: String?, actionTitle: String, handler: @escaping (_ text: String?)->() ) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alertController.addTextField { (textField) in
            textField.text = fieldText
        }
        let actionOK = UIAlertAction(title: actionTitle, style: .default) { (_) in
            handler(alertController.textFields?.first?.text)
        }
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(actionOK)
        alertController.addAction(actionCancel)
        DispatchQueue.main.async {
            UIApplication.topViewController()!.present(alertController, animated: true, completion: nil)
        }
    }

}
