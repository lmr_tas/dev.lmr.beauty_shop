//
//  Constants.swift
//
//
//  Created by Alexey Trotsenko on 29.11.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import Parse
import Foundation

enum ParseClasses: String {
    case Category
    case Service
}

typealias EmptyBlock                = ()->()
typealias FailureErrorBlock         = (_ errorString: String)->()
typealias UploadImageResultBlock    = (_ result: Bool, _ fileURL: String?, _ file: PFFile?, _ error: Error?)->()
typealias OperationResultBlock      = (_ result: Bool, _ error: Error?)->()
typealias UserLoginResultBlock      = (_ user: User?, _ error: Error?)->()
typealias UserFBLoginResultBlock    = (_ user: User?, _ error: Error?, _ isNew: Bool)->()
//typealias FetchedObjectsResultBlock = <T:PFObject]>(([T]?, Error?) -> ())?



//TODO: - NOTES
/*
 Add to service how many viewed
 
 */


/*
 Since this is a known bug in parse, here's a workaround that I implemented to address this issue:
 
 This bug does not apply to whereKey:withinGeoBoxFromSouthwest:toNortheast: . So, we want to create a bounding box around the circle in which we want to search. We'll do it this way (taken from another SO question)
 
 CLLocationCoordinate2D centerCoord = CLLocationCoordinate2DMake(currentUser.location.latitude, currentUser.location.longitude);
 
 MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(centerCoord, 2*currentUser.radius*1000 , 2*currentUser.radius*1000);
 
 double latMin = region.center.latitude - .5 * region.span.latitudeDelta;
 double latMax = region.center.latitude + .5 * region.span.latitudeDelta;
 double lonMin = region.center.longitude - .5 * region.span.longitudeDelta;
 double lonMax = region.center.longitude + .5 * region.span.longitudeDelta;
 
 PFGeoPoint *southwest = [PFGeoPoint geoPointWithLatitude:latMin longitude:lonMin];
 PFGeoPoint *northeast = [PFGeoPoint geoPointWithLatitude:latMax longitude:lonMax];
 Now, we can use the other method to retrieve the data. However, we only want data from within a circle around the user, not a box - so this returns too many results (and also, it may have problems around the poles, keep that in mind)
 
 To overcome this, we iterate through self.objects in objectsDidLoad: and remove every object that is too far away.
 
 However, we cannot overwrite self.objects. Thus, we create another property called self.validObjects, which we will use to store the actual values. To get this working, we also overwrite the following two methods (here shown for my simple case with just one section):
 
 - (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
 return self.validObjects.count;
 }
 - (PFObject*) objectAtIndexPath:(NSIndexPath *)indexPath{
 return self.validObjects[indexPath.row];
 }
 */
