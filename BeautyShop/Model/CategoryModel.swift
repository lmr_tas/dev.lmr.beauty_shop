//
//  CategoryModel.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 12.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import Foundation
import Parse

class Category: PFObject, PFSubclassing {

    static func parseClassName() -> String {
        return ParseClasses.Category.rawValue
    }
    
    @NSManaged var name: String
    
}
