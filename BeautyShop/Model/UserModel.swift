//
//  UserModel.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 12.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import Foundation
import Parse

class User: PFUser {
    
    enum UserType: Int {
        case customer
        case master
    }
    
    @NSManaged fileprivate var type: NSNumber?

    @NSManaged var name: String?
    @NSManaged var avatar: PFFile?
    
    @NSManaged var location: PFGeoPoint?
    @NSManaged var radius: Double
    @NSManaged var address: String?

    @NSManaged var categories: [Category]?
    @NSManaged var services: PFRelation<Service>?



    var userTypeValue: UserType? {
        get {
            if let userType = type {
                return UserType(rawValue: userType.intValue)
            }
            return nil
        }
        set {
            if let value = newValue {
                self.type = NSNumber(integerLiteral: value.rawValue)
            }
        }
        
//        get {
//            return (self.userType?.intValue).map{ return User.UserType(rawValue: $0) } ?? nil
//        }
//
//        set {
//            self.userType = newValue.map({ NSNumber(integerLiteral:$0.rawValue) })
//        }

    }
    
    func addRelation(_ service: Service) {
        self.relation(forKey: "services").add(service)
    }
    
    var coordinate: CLLocationCoordinate2D? {
        if let location = location {
            return CLLocationCoordinate2D(latitude: location.latitude, longitude: location.longitude)
        }
        return nil
    }
}

