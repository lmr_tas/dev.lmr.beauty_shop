//
//  ServiceModel.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 28.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import Foundation
import Parse

class Service: PFObject, PFSubclassing {
    
    static func parseClassName() -> String {
        return ParseClasses.Service.rawValue
    }
    
    @NSManaged var name: String
    @NSManaged var price: Double
    @NSManaged var duration: Int
    @NSManaged var descr: String?
    @NSManaged var owner: PFUser
    @NSManaged var image: PFFile?
    @NSManaged var category: Category
    @NSManaged var location: PFGeoPoint?

    
    required init(name: String, price: Double = 0, duration: Int = 0, description: String?, category: Category) {
        super.init()
        
        self.name = name
        self.price = price
        self.duration = duration
        self.descr = description
        self.category = category
        if let user = User.current() {
            self.owner = user
            self.location = user.location
            let acl = PFACL()
            acl.setWriteAccess(true, for: user)
            acl.getPublicReadAccess = true
            self.acl = acl
        }
    }

    
    override init() {
        super.init()
    }
}
