//
//  LocationManager.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 9/25/17.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit
import CoreLocation

protocol LocationDelegate {
    func locationUpdated(_ locationManager: LocationManager, coordinate: CLLocationCoordinate2D?, error: Error?)
}

enum LocationErorr: Error {
    case getEnteredAddress
    case findingAddress

    var localizedDescription: String {
        switch self {
        case .getEnteredAddress: return "Your entered location not found"
        case .findingAddress: return "Your location not found"
        }
    }
}

class LocationManager: NSObject {

    var delegate: LocationDelegate?
    var currentZip: String?
    var currentAddress: String?
    var currentCity: String?
    
    var coordinate: CLLocationCoordinate2D?
    var isFirst: Bool = true
    let locationManager = CLLocationManager()
    let testLocation = CLLocation(latitude: 47.8504856, longitude: 35.1146213) //Test location

    override init() {
        super.init()
        //self.getLocation()
    }
    
    //MARK: - Locations methods
    func getLocation() -> Bool {
        isFirst = true
        if CLLocationManager.locationServicesEnabled() {
            switch(CLLocationManager.authorizationStatus()) {
            case .notDetermined:
                locationManager.requestWhenInUseAuthorization()
            case .restricted, .denied:
                self.update(testLocation)
                return false
            case .authorizedAlways, .authorizedWhenInUse:
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyBest
                locationManager.startUpdatingLocation()
                return true
            }
        } else {
            print("Location services are not enabled")
        }
        return false
    }
    
    func getLocationFrom(_ address: String, completionHandler: @escaping (_ coordinate: CLLocationCoordinate2D?, _ error: Error?) -> ()) {
        CLGeocoder().geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
            let coordinate = placemarks?.first?.location!.coordinate
            self.coordinate = coordinate
            completionHandler(coordinate, error)
        })
    }
}

//MARK: - CLLocationManagerDelegate
extension LocationManager: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        manager.stopUpdatingLocation()
        print("[location manager stopUpdatingLocation]")
        self.update(locations[0])

    }
    //TODO: Разобраться с двойным вызовом поиска локации
    private func update(_ location: CLLocation) {
        
        self.coordinate = location.coordinate
        
        CLGeocoder().reverseGeocodeLocation(location, completionHandler: { (placemarks, error) -> Void in
            if !self.isFirst { return }
            if error != nil {
                self.delegate?.locationUpdated(self, coordinate: nil, error: error)
                print("[Find City Error]: ", error?.localizedDescription as Any)
                return
            }
            guard let myAddress = placemarks?.last else {
                print("[No City]: ", error?.localizedDescription as Any)
                return
            }
            if let zipCode = myAddress.postalCode as String? {
                self.currentZip = zipCode
            }
            if let address = myAddress.locality, let street = myAddress.name {
                self.currentAddress = address + ", " + street
                self.currentCity = address
            }
            print("[location manager founded]")
            self.delegate?.locationUpdated(self, coordinate: location.coordinate, error: nil)
            self.isFirst = false
        })
    }
}
