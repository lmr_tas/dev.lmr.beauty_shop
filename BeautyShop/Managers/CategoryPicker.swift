//
//  CategoryPicker.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 12.01.2018.
//  Copyright © 2018 Alexey Trotsenko. All rights reserved.
//

import Foundation
import ActionSheetPicker_3_0

class CategoryPicker {
    
    static func showPickerWith(_ categories: [Category], position: Int, completionHandler: @escaping (_ index: Int?, _ category: Category?, _ error: Error?) -> ()) {
        ClientAPI.shared.fetchObjects(categories) { (categories, error) in
            if let error = error {
                return completionHandler(nil, nil, error)
            }
            //TODO: - Need test optional mapping
            let categoryNames = categories?.map{ $0.name }
            DispatchQueue.main.async {
                ActionSheetStringPicker.show(withTitle: "Chose category", rows: categoryNames, initialSelection: position, doneBlock: { (picker, index, object) in
                    completionHandler(index, categories?[index], nil)
                }, cancel: { (picker) in
                }, origin: UIApplication.topViewController()?.view)
            }
        }
    }
}
