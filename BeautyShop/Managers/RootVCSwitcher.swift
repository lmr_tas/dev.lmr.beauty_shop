//
//  RootVCSwitcher.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 21.11.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import Foundation
import UIKit

enum Controllers: String {
    //Storyboards:
    // Main
    case MainTabBarViewController
    case MoreViewController

    //Login
    case LoginNavigationController
    
    //Profile
    case ProfileNavigationController

    //Search
    case HomeNavigationController

    //Service
    case MasterServiceNavigationController
}



struct RootVCSwitcher {
    
    private let storyboardsNames = ["Main", "Search", "Login", "Profile", "Service", "Chat"]
    
    public func switchToRootController(_ controller: Controllers, with animation: UIViewAnimationOptions) {
        guard let window = UIApplication.shared.delegate?.window as? UIWindow,
            let rootVC = window.rootViewController,
            let destController = getController(controller.rawValue) else {
                UIApplication.showAlertMessage("Change screen error")
                return
        }
        print("ROOT SWITCHER")
        print("[From \(rootVC) to \(destController)]")
        DispatchQueue.main.async {
            UIView.transition(from: rootVC.view, to: destController.view,
                              duration: 0.3,
                              options: animation) { (finished) in
                                window.rootViewController = destController
                                window.makeKeyAndVisible()
            }
        }
    }
    
    private func getController(_ controller: String) -> UIViewController? {
        for storyboard in storyboardsNames {
            let storyboard = UIStoryboard(name: storyboard, bundle: nil)
            if let availableIdentifiers = storyboard.value(forKey: "identifierToNibNameMap") as? [String: Any] {
                if availableIdentifiers[controller] != nil {
                    let controller = storyboard.instantiateViewController(withIdentifier: controller)
                    return controller
                }
            }
        }
        return nil
    }
    
    
}


