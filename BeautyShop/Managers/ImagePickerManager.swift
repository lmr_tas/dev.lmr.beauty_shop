//
//  ImageManager.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 04.01.2018.
//  Copyright © 2018 Alexey Trotsenko. All rights reserved.
//

import UIKit

protocol ImagePickerManagerDelegate {
    func pickedImage(_ image: UIImage?)
}


class ImagePickerManager: NSObject {
    
    var delegate: ImagePickerManagerDelegate?
    lazy var viewController = UIApplication.topViewController()

    
    func presentImagePicker() {
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let cameraAction = UIAlertAction(title: "Camera", style: .default) { (action) in
            self.pickImageAction(source: .camera) }
        let galeryAction = UIAlertAction(title: "Galery", style: .default) { (action) in
            self.pickImageAction(source: .photoLibrary) }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addAction(cameraAction)
        alertController.addAction(galeryAction)
        alertController.addAction(cancelAction)
        
        viewController?.present(alertController, animated: true, completion: nil)
    }
    
    fileprivate func pickImageAction(source: UIImagePickerControllerSourceType) {
        
        if source == .camera && !UIImagePickerController.isSourceTypeAvailable(.camera) {
            UIApplication.showAlertMessage("Sorry, your device has No camera")
            return
        }
        let imagePicker = UIImagePickerController()
        imagePicker.delegate = self
        imagePicker.allowsEditing = true
        imagePicker.sourceType = source
        viewController?.present(imagePicker, animated: true) {
            UIApplication.hideHUD()
        }
        UIApplication.showHUD(withMask: .black)
    }
}

extension ImagePickerManager: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        picker.dismiss(animated: true) {
            self.delegate?.pickedImage(info[UIImagePickerControllerEditedImage] as? UIImage)
        }
    }
}
