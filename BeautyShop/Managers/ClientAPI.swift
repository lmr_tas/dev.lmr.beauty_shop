//
//  ClientAPI.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 13.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import Foundation
import ParseFacebookUtilsV4
import FBSDKLoginKit
import Parse

enum CustomErorr: Error {
    case imageSerialization
    case modelSerialization
    case queryNil
    case deleteObject
    case unauthorized
    
    var localizedDescription: String {
        switch self {
        case .imageSerialization: return "Serialization"
        case .unauthorized: return "Unauthorized"
        case .modelSerialization: return "ModelSerialization"
        case .deleteObject: return "Delete the object error"
        case .queryNil: return "Can't find objects"
        }
    }
}

class ClientAPI {
    static let shared = ClientAPI()
    let updatedAt = "updatedAt"
}

// MARK: - User login
extension ClientAPI {
    
    var isLoggedIn: Bool {
        return User.current() != nil
    }
    
    func signIn(_ email: String, _ password: String, completionHandler: UserLoginResultBlock?) {
        PFUser.logInWithUsername(inBackground: email, password: password) { (user, error) in
            completionHandler?(user as? User, error)
        }
    }
    
    func signUp(email: String, password: String, completionHandler: @escaping OperationResultBlock) {
        let user = User()
        user.username = email
        user.email = email
        user.password = password
        user.signUpInBackground { (isSuccess, error) in
            completionHandler(isSuccess, error)
        }
    }

    func loginViaFacebook(completionHandler: UserFBLoginResultBlock?) {
        PFFacebookUtils.logInInBackground(withReadPermissions: ["public_profile"]) { user, error in
            guard let currentUser = user as? User else {
                return
            }
//            //TODO: Test property isNew
//            if !currentUser.isNew {
//                return completionHandler!(currentUser, nil, false)
//            }
            
            if currentUser.userTypeValue != nil {
                return completionHandler!(currentUser, nil, false)
            }
            
            self.loadCurrentUserData(completionHandler: { (email, name, avatarPath, error) in
                guard error == nil else {
                    completionHandler?(nil, error, true)
                    return
                }
                if let avatarPath = avatarPath,
                    let url = URL(string: avatarPath),
                    let data = try? Data(contentsOf: url),
                    let image = UIImage(data: data) {
                    
                    self.upload(image: image, completion: { (isSuccess, url, file, error) in
                        currentUser.name = name
                        currentUser.avatar = file
                        currentUser.saveInBackground(block: { (isSuccess, error) in
                            guard isSuccess else {
                                completionHandler?(nil, error, true)
                                return
                            }
                            completionHandler?(currentUser, nil, true)
                        })
                    })
                } else {
                    currentUser.name = name
                    currentUser.saveInBackground(block: { (isSuccess, error) in
                        guard isSuccess else {
                            completionHandler?(nil, error, true)
                            return
                        }
                        completionHandler?(currentUser, nil, true)
                    })
                }
            })
        }
    }
    
    func updateUserProfile(with avatar: UIImage? = nil, completionHandler: @escaping OperationResultBlock) {
        if let image = avatar {
            self.upload(image: image, completion: { (result, path, file, error) in
                guard result else {
                    completionHandler(false, error)
                    return
                }
                User.current()?.avatar = file
                User.current()?.saveInBackground { (isSuccess, error) in
                    completionHandler(isSuccess, error)
                }
            })
        } else {
            User.current()?.saveInBackground { (isSuccess, error) in
                completionHandler(isSuccess, error)
            }
        }
    }
}

// MARK: - Save data
extension ClientAPI {
    func updateService(_ service: Service, image: UIImage?, completionHandler: @escaping OperationResultBlock) {
        if let image = image {
            upload(image: image, completion: { (success, URL, file, error) in
                if let error = error {
                    return completionHandler(false, error)
                }
                service.image = file
                service.saveInBackground(block: { (success, error) in
                    completionHandler(success, error)
                })
            })
        } else {
            service.saveInBackground(block: { (success, error) in
                completionHandler(success, error)
            })
        }
    }
}

// MARK: - Fetch data
extension ClientAPI {
    
    func loadServices(category: Category?, fromIndex: Int?, completionHandler: @escaping (([Service]?, _ count: Int32?, _ error: Error?) -> ())) {
        
        let query = Service.query()
        if let category = category {
            query?.whereKey("category", equalTo: category)
        }
        if let skip = fromIndex {
            query?.skip = skip
        }
        self.addLocationFilter(query)
        query?.addDescendingOrder(updatedAt)
        query?.countObjectsInBackground(block: { (count, error) in
            if let error = error {
                return completionHandler(nil, count, error)
            }
            query?.findObjectsInBackground(block: { (services, error) in
                completionHandler(services as? [Service], count, error)
            })
        })
    }
    
    func loadMasterServices(_ user: User?, completionHandler: @escaping ([Service]?, Error?)->()) {
        guard let query = user?.services?.query() else {
            return completionHandler(nil, CustomErorr.queryNil)
        }
        query.addDescendingOrder(updatedAt)
        query.findObjectsInBackground { (objects, error) in
            completionHandler(objects, error)
        }
    }
    
    func loadMasters(completionHandler: @escaping ([User]?, Error?) -> ()) {
        let query = User.query()
        query?.whereKey("type", equalTo: 1)
        if let user = User.current(), let id = user.objectId {
            query?.whereKey("objectId", notEqualTo: id)
        }
        query?.findObjectsInBackground(block: { (objects, error) in
            if let error = error {
                return completionHandler(nil, error)
            }
            guard let masters = objects as? [User], !masters.isEmpty else {
                return completionHandler(nil, CustomErorr.queryNil)
            }
            completionHandler(masters, nil)
        })
    }
    
    func loadCategories(completionHandler: (([Category]?, Error?) -> ())?) {
        let query = Category.query()?.fromLocalDatastore()
        query?.findObjectsInBackground() { categories, error in
            
            if let categories = categories, categories.isEmpty {
                let query = Category.query()
                query?.findObjectsInBackground() { categories, error in
                    completionHandler?(categories as? [Category], error)
                }
            }
            completionHandler?(categories as? [Category], error)
        }
    }
    
    func fetchObjects<T: PFObject>(_ objects: [T], completionHandler: @escaping ([T]?, Error?) -> ()){
        T.fetchAllIfNeeded(inBackground: objects) { (fetchedObjects, error) in
            completionHandler(fetchedObjects as? [T], error)
        }
    }
    
    func fetchObject<T: PFObject>(_ object: T, completionHandler: @escaping (T?, Error?) -> ()){
        object.fetchIfNeededInBackground { (fetchedObject, error) in
            completionHandler(fetchedObject as? T, error)
        }
    }
    
    private func addLocationFilter(_ query: PFQuery<PFObject>?) {
        if let user = User.current(), let loc = user.location, Double(user.radius) > 0 {
           query?.whereKey("location", nearGeoPoint: loc, withinKilometers: Double(user.radius))
        }
    }
}


// MARK: - Delete data
extension ClientAPI {
    func deleteMasterService(_ service: Service, completionHandler: @escaping OperationResultBlock) {
        service.deleteInBackground { (success, error) in
            completionHandler(success, error)
        }
    }
}


// MARK: - Data uploading
extension ClientAPI {
    func upload(image: UIImage, completion: @escaping UploadImageResultBlock, progress: PFProgressBlock? = nil) {
        guard let imageData = UIImageJPEGRepresentation(image, 0.7) , let file = PFFile(data: imageData) else {
            return completion(false, nil, nil, CustomErorr.imageSerialization)
        }
        file.saveInBackground({ (result, error) in
            completion(result, file.url, file, error)
        }) { (progressValue) in
            progress.map({ $0(progressValue) })
        }
    }
}

// MARK: - Local Data Store
extension ClientAPI {
    func downloadCategoriesForLocalUse() {
        self.loadCategories { (categories, error) in
            if error == nil {
                PFObject.pinAll(inBackground: categories, block: { (success, error) in
                    print("[PinnedCategories] ", categories as Any)
                })
            }
        }
    }
}

// MARK: - FB load user data
extension ClientAPI {
    fileprivate var facebookUserProfileAvatarParams: Dictionary<String, Any> {
        return ["redirect" : false, "height" : 1024, "width" : 1024, "type" : "large"]
    }
    
    typealias UserDataBlock = (_ email: String?, _ name: String?, _ avatar: String?, _ error: Error?) -> Void
    
    fileprivate func loadCurrentUserData(completionHandler: UserDataBlock?) {
        let userProfileParameters = ["fields" : "id, email, name, picture"]
        let userProfileRequest = FBSDKGraphRequest(graphPath: "me", parameters: userProfileParameters)
        let graphConnection = FBSDKGraphRequestConnection()
        
        graphConnection.add(userProfileRequest, completionHandler: { _, result, error in
            let userProfilePictureRequest = FBSDKGraphRequest(graphPath: "me/picture",
                                                              parameters: self.facebookUserProfileAvatarParams,
                                                              httpMethod: "GET")
            
            _ = userProfilePictureRequest?.start(completionHandler: { _, avatarResponse, _ in
                guard let results = result as? [String: Any], (results["id"] as? String) != nil else {
                    completionHandler?(nil, nil, nil, error)
                    return
                }
                
                let emailAddress = results["email"] as? String
                let fullName = results["name"] as? String
                let avatarPath = ((avatarResponse as! [String: Any])["data"] as! [String: Any])["url"] as? String
                completionHandler?(emailAddress, fullName, avatarPath, nil)
            })
        })
        graphConnection.start()
    }
}
