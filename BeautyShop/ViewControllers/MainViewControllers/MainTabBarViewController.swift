//
//  MainTabBarViewController.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 15.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit

class MainTabBarViewController: UITabBarController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        updateVisibleTabs()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    func updateVisibleTabs() {
        guard let tabControllers = self.viewControllers else {
            return
        }
        if User.current()?.userTypeValue != User.UserType.master {
            for (index, controller) in tabControllers.enumerated() {
                if controller.restorationIdentifier == Controllers.MasterServiceNavigationController.rawValue {
                    self.viewControllers?.remove(at: index)
                }
            }
        }
    }
    
}
