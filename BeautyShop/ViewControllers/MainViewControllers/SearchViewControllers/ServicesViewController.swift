//
//  ServicesViewController.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 12.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit

class ServicesViewController: BaseViewController {

    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var pickerEnablerView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    
    lazy var refreshControl = UIRefreshControl()

    var categories: [Category]?
    var services:   [Service]? {
        didSet {
            print(services?.count as Any, allServicesCount)
        }
    }
    var allServicesCount: Int32 = 0
    var refreshCount: Int {
        if let serviceCount = self.services?.count {
            return serviceCount - 1
        }
        return 0
    }
    var selectedCategory: Category? {
        willSet {
            if let newValue = newValue {
                categoryLabel.text = newValue.name
            } else {
                categoryLabel.text = "All"
            }
        }
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Services"
        self.collectionView.alwaysBounceVertical = true
        collectionView.registerCell(ServiceCollectionViewCell.self)
        activateGestureRecognizer()
        getCategory()
        getServices()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        
//        collection cell layout setup
//        other settings (insets) in storyboard
        let layout = collectionView.collectionViewLayout as! UICollectionViewFlowLayout
        let bounds = UIEdgeInsetsInsetRect(view.bounds, layout.sectionInset)
        let sideLength = min(bounds.width, bounds.height) / 2.0 - layout.minimumInteritemSpacing
        layout.itemSize = CGSize(width: sideLength, height: sideLength)
    }
    
    // MARK: - Actions
    @objc func showCategoryPicker(tapGestureRecognizer: UITapGestureRecognizer) {
        guard let categories = categories, !categories.isEmpty else {
            return getCategory()
        }
        let index: Int = 0
        
        CategoryPicker.showPickerWith(categories, position: index) { (index, category, error) in
            if let error = error {
                return self.error(error.localizedDescription)
            }
            self.selectedCategory = category
            self.getServices {
                self.collectionView.scrollToTop(animated: false)
            }
        }
    }
    
    @objc func refresh(sender: AnyObject) {
        getServices() {
            DispatchQueue.main.async {
                self.refreshControl.endRefreshing()
            }
        }
    }
    @IBAction func resetAction(_ sender: UIButton) {
        selectedCategory = nil
        getServices {
            self.collectionView.scrollToTop(animated: false)
        }
    }
    
    // MARK: - Private
    private func activateGestureRecognizer() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showCategoryPicker(tapGestureRecognizer:)))
        pickerEnablerView.addGestureRecognizer(tapGestureRecognizer)
        refreshControl.addTarget(self, action: #selector(refresh), for: UIControlEvents.valueChanged)
        collectionView.addSubview(refreshControl)
    }
    
    
    private func getServices(fromIndex: Int = 0, completionHandler: EmptyBlock? = nil) {
        self.showLoader(mask: .none)
        ClientAPI.shared.loadServices(category: selectedCategory, fromIndex: fromIndex) { (services, count, error) in
            self.hideLoader()
            completionHandler?()
            
            if let error = error {
                return self.error(error.localizedDescription)
            }
            
            guard let services = services, let count = count else {
                return self.error("Get services from server error")
            }
            
            self.allServicesCount = count
            self.services = fromIndex > 0 && self.services != nil ? self.services! + services : services
            self.collectionView.reload()
        }
    }
    
    private func getCategory() {
        self.showLoader()
        ClientAPI.shared.loadCategories { (categories, error) in
            self.hideLoader()
            if let error = error {
                return self.error(error.localizedDescription)
            }
            guard let categories = categories else {
                return self.error("Get category from server error")
            }
            self.categories = categories
        }
    }
}

// MARK: - UICollectionViewDataSource
extension ServicesViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return  services?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueCell(cls: ServiceCollectionViewCell.self, indexPath: indexPath),
         let service = services?[indexPath.row] else {
            return UICollectionViewCell()
        }
        cell.cellSource(service)
        return cell
    }
}

// MARK: UICollectionViewDelegate
extension ServicesViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == refreshCount && services!.count < allServicesCount {
            getServices(fromIndex: services!.count)
        }
    }
}

