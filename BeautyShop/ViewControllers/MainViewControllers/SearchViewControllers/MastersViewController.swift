//
//  MastersViewController.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 12.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit
import MapKit

class MastersViewController: BaseViewController {
    
    @IBOutlet weak var mapView: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Masters"
        getMasters()
    }
    
    private func getMasters() {
        ClientAPI.shared.loadMasters { (masters, error) in
            if let error = error {
                return self.error(error.localizedDescription)
            }
            guard let masters = masters else {
                return self.error("Get masters from server error")
            }
            let annotations = masters.map{MasterAnnotation($0)} as! [MasterAnnotation]
            self.mapView.addAnnotations(annotations)
            self.mapView.showAnnotations(annotations, animated: true)
        }
    }
}

extension MastersViewController: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        if annotation is MKUserLocation {
            return nil
        }
        let identifire = "masterAnnotation"
        var annotationView = mapView.dequeueReusableAnnotationView(withIdentifier: identifire)
        if annotationView == nil {
            annotationView = MasterAnnotationView(annotation: annotation, reuseIdentifier: identifire)
            (annotationView as! MasterAnnotationView).masterDetailDelegate = self
        } else {
            annotationView!.annotation = annotation
        }
        
        return annotationView
    }
}

extension MastersViewController: MasterDetailMapViewDelegate {
    func detailsRequested(_ master: User) {
        print("Selected \(master)")
    }
}

