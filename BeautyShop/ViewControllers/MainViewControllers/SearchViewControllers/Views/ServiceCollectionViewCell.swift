//
//  ServiceCollectionViewCell.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 12.01.2018.
//  Copyright © 2018 Alexey Trotsenko. All rights reserved.
//

import UIKit
import ParseUI

class ServiceCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var imageView: CustomPFImageView!
    @IBOutlet weak var priceLabel: UILabel!
    
    func cellSource(_ service: Service) {
        if service.image != nil {
            imageView.file = service.image
        } else {
            imageView.image = UIImage(named: "BS")
        }
        
        priceLabel.text = String(format: "%.2f $", service.price)
        nameLabel.text = service.name
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
    }
}
