//
//  MasterAnnotationView.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 07.02.2018.
//  Copyright © 2018 Alexey Trotsenko. All rights reserved.
//

import UIKit
import MapKit

class MasterAnnotation: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    var master: User
    var title: String?
    
    init?(_ user: User) {
        guard let coordinate = user.location?.coordinate else {
            return nil
        }
        self.coordinate = coordinate
        self.title = user.name
        self.master = user
        super.init()
    }
}

class MasterAnnotationView: MKAnnotationView {

    weak var masterDetailDelegate: MasterDetailMapViewDelegate?
    weak var customCalloutView: MasterDetailAnnotationView?
    
    override var annotation: MKAnnotation? {
        willSet {
            customCalloutView?.removeFromSuperview()
        }
    }
    
    // MARK: - life cycle
    
    override init(annotation: MKAnnotation?, reuseIdentifier: String?) {
        super.init(annotation: annotation, reuseIdentifier: reuseIdentifier)
        self.canShowCallout = false
        self.image = #imageLiteral(resourceName: "Flag")
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.canShowCallout = false
        self.image = #imageLiteral(resourceName: "Flag")
    }
    
    // MARK: - callout showing and hiding
    // Important: the selected state of the annotation view controls when the
    // view must be shown or not. We should show it when selected and hide it
    // when de-selected.
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        if selected {
            self.customCalloutView?.removeFromSuperview() // remove old custom callout (if any)
            
            if let newCustomCalloutView = loadPersonDetailMapView() {
                // fix location from top-left to its right place.
                newCustomCalloutView.frame.origin.x -= newCustomCalloutView.frame.width / 2.0 - (self.frame.width / 2.0)
                newCustomCalloutView.frame.origin.y -= newCustomCalloutView.frame.height
                
                // set custom callout view
                self.addSubview(newCustomCalloutView)
                self.customCalloutView = newCustomCalloutView
                
                // animate presentation
                if animated {
                    self.customCalloutView!.alpha = 0.0
                    UIView.animate(withDuration: 0.3, animations: {
                        self.customCalloutView!.alpha = 1.0
                    })
                }
            }
        } else {
            if customCalloutView != nil {
                if animated { // fade out animation, then remove it.
                    UIView.animate(withDuration: 0.3, animations: {
                        self.customCalloutView!.alpha = 0.0
                    }, completion: { (success) in
                        self.customCalloutView!.removeFromSuperview()
                    })
                } else {
                    self.customCalloutView!.removeFromSuperview()
                } // just remove it.
            }
        }
    }
    
    func loadPersonDetailMapView() -> MasterDetailAnnotationView? {
        if let views = Bundle.main.loadNibNamed("MasterDetailAnnotationView", owner: self, options: nil) as? [MasterDetailAnnotationView], views.count > 0 {
            let masterDetailAnnotation = views.first!
            masterDetailAnnotation.delegate = self.masterDetailDelegate
            if let masterAnnotation = annotation as? MasterAnnotation {
                let master = masterAnnotation.master
                masterDetailAnnotation.viewSource(master)
            }
            return masterDetailAnnotation
        }
        return nil
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        self.customCalloutView?.removeFromSuperview()
    }

    // MARK: - Detecting and reaction to taps on custom callout.
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        // if super passed hit test, return the result
        if let parentHitView = super.hitTest(point, with: event) { return parentHitView }
        else { // test in our custom callout.
            if customCalloutView != nil {
                return customCalloutView!.hitTest(convert(point, to: customCalloutView!), with: event)
            } else { return nil }
        }
    }
}
