//
//  MasterDetailAnnotationView.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 07.02.2018.
//  Copyright © 2018 Alexey Trotsenko. All rights reserved.
//

import UIKit
import ParseUI


protocol MasterDetailMapViewDelegate: class {
    func detailsRequested(_ master: User)
}

class MasterDetailAnnotationView: UIView {

    @IBOutlet weak var backgroundContentButton: UIButton!
    @IBOutlet weak var avatarImageView: CustomPFImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoriesTextView: UITextView!
    @IBOutlet weak var seeDetailsButton: UIButton!
    
    var master: User!
    var delegate: MasterDetailMapViewDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        backgroundContentButton.applyArrowDialogAppearanceWithOrientation(arrowOrientation: .down)
    }
    // MARK: IBAction
    @IBAction func showDetailAction(_ sender: Any) {
        delegate?.detailsRequested(master)
    }
    
    func viewSource(_ master: User) {
        self.master = master
        avatarImageView.file = master.avatar

        nameLabel.text = master.name
        categoriesTextView.text = master.categories?.map{$0.name}.joined(separator: " \n")
    }
    
    // MARK: - Hit test. We need to override this to detect hits in our custom callout.
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        // Check if it hit our annotation detail view components.
        
        // details button
        if let result = seeDetailsButton.hitTest(convert(point, to: seeDetailsButton), with: event) {
            return result
        }
        // list
        if let result = categoriesTextView.hitTest(convert(point, to: categoriesTextView), with: event) {
            return result
        }
        // fallback to our background content view
        return backgroundContentButton.hitTest(convert(point, to: backgroundContentButton), with: event)
    }
}
