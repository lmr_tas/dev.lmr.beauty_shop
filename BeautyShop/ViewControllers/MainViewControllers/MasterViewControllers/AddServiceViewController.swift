//
//  AddServiceViewController.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 04.01.2018.
//  Copyright © 2018 Alexey Trotsenko. All rights reserved.
//

import UIKit
import ActionSheetPicker_3_0
import Parse

protocol AddServiceDelegate {
    func serviceChanged()
}


class AddServiceViewController: BaseViewController {
    
    lazy var imageManager = ImagePickerManager()

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var decriptionTextView: UITextView!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var durationTextField: UITextField!
    
    var delegate: AddServiceDelegate?
    var service: Service?
    var serviceCategory: Category? {
        willSet {
            categoryLabel.text = newValue?.name
        }
    }
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        updateUI()
        activateGestureRecognizer()
    }

    // MARK: - Private
    private func updateUI() {
        guard let _ = service else { return }
        imageView.imageFromPFFile(file: service?.image)
        nameTextField.text = service?.name
        decriptionTextView.text = service?.descr
        priceTextField.text = "\(service!.price)"
        durationTextField.text = "\(service!.duration)"
        
        service?.category.fetchIfNeededInBackground(block: { (category, error) in
            if error == nil {
                self.serviceCategory = self.service?.category
            }
        })
    }
    
    private func activateGestureRecognizer() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(showCategoryPicker(tapGestureRecognizer:)))
        categoryLabel.addGestureRecognizer(tapGestureRecognizer)
    }
    
    // MARK: - Actions
    @objc func showCategoryPicker(tapGestureRecognizer: UITapGestureRecognizer) {
        guard let userCategories = User.current()?.categories else {
            return
        }
        var index: Int = 0
        if let idx = userCategories.indexOf(self.service?.category) { index = idx }
        
        CategoryPicker.showPickerWith(userCategories, position: index) { (index, category, error) in
            if let error = error {
                return self.error(error.localizedDescription)
            }
            self.serviceCategory = category
        }

//        ClientAPI.shared.fetchObjects(userCategories) { (categories, error) in
//            self.categoryLabel.isUserInteractionEnabled = true
//            if let error = error {
//                return self.error(error.localizedDescription)
//            }
//            //TODO: - Need test optional mapping
//            let categoryNames = categories?.map{ $0.name }
//            var index: Int = 0
//            if let idx = categories?.indexOf(self.service?.category) { index = idx }
//            DispatchQueue.main.async {
//                ActionSheetStringPicker.show(withTitle: "Chose category", rows: categoryNames, initialSelection: index, doneBlock: { (picker, index, object) in
//                    self.serviceCategory = categories?[index]
//                }, cancel: { (picker) in
//                }, origin: self.view)
//            }
//        }
    }
    
    @IBAction func addImageAction(_ sender: Any) {
        imageManager.delegate = self
        imageManager.presentImagePicker()
    }
    
    @IBAction func saveServiceAction(_ sender: Any) {
        guard let selectedCat = self.serviceCategory else {
            return error("Please select category")
        }
        guard let name = nameTextField.text,
        let price = Double(priceTextField.text ?? "0"),
            let duration = Int(durationTextField.text ?? "0") else {
                return error("Please fill all fields")
        }
        if name.isEmpty {
            return error("Please enter name")
        }
        
        if service == nil {
            service = Service(name: name,
                                  price: price,
                                  duration: duration,
                                  description: decriptionTextView.text,
                                  category: selectedCat)
        } else {
            service?.name = name
            service?.descr = decriptionTextView.text
            service?.price = price
            service?.duration = duration
            service?.category = selectedCat
        }

        guard let service = self.service else {
            return error("Generate service error try later")
        }
        
        showLoader()
        ClientAPI.shared.updateService(service, image: imageView.image) { (success, error) in
            User.current()?.addRelation(service)
            ClientAPI.shared.updateUserProfile(completionHandler: { (success, error) in
                self.hideLoader()
                self.delegate?.serviceChanged()
                self.navigationController?.popViewController(animated: true)
            })
        }
    }
}

extension AddServiceViewController: ImagePickerManagerDelegate {
    func pickedImage(_ image: UIImage?) {
        imageView.image = image
    }
}


