//
//  ServiceTableViewCell.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 28.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit
import ParseUI

class ServiceTableViewCell: UITableViewCell {

    @IBOutlet weak var serviceImage: CustomPFImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var durationLabel: UILabel!
    @IBOutlet weak var descrLabel: UILabel!
    
    func cellSource(_ service: Service) {
        
        serviceImage.file = service.image
        
        nameLabel.text = service.name
        priceLabel.text = String(format: "%.2f", service.price)
        durationLabel.text = "\(service.duration)"
        descrLabel.text = service.descr
    }

}
