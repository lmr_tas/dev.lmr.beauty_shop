//
//  MasterServiceViewController.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 28.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit

class MasterServiceViewController: BaseViewController {
    
    enum Segue: String {
        case EditServiceSegue
        case AddServiceSegue
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var editButton: UIBarButtonItem!
    
    var services = [Service]()
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        updateUI()
        loadServices()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    // MARK: - IBAction
    @IBAction func editDoneButtonAction(_ sender: UIBarButtonItem) {
        tableView.setEditing(!tableView.isEditing, animated: true)
        
        if tableView.isEditing {
            self.editButton.title = "Done"
        } else {
            self.editButton.title = "Edit"
        }
    }
    
    // MARK: - Private
    fileprivate func updateUI() {
        tableView.registerCell(ServiceTableViewCell.self)
        tableView.rowAutoHeight(estimatedRowHeight: 60)
    }
    
    private func loadServices() {
        showLoader()
        ClientAPI.shared.loadMasterServices(User.current()) { (objects, error) in
            self.hideLoader()
            if let _ = error as? CustomErorr {
                return
            }
            if let error = error {
                return self.error(error.localizedDescription)
            }
            if let service = objects {
                self.services = service
                self.tableView.reload()
            }
        }
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        guard let controller = segue.destination as? AddServiceViewController,
            let segueIdentifier = segue.identifier,
            let segueID = Segue(rawValue: segueIdentifier) else { return }
        controller.delegate = self
        
        switch segueID {
        case .EditServiceSegue:
            guard let tableView = sender as? UITableView,
                let indexPath = tableView.indexPathForSelectedRow else { return }
            controller.service = services[indexPath.row]
            tableView.deselectRow(at: indexPath, animated: true)
        case .AddServiceSegue: break
        }
    }
}

// MARK: - UITableViewDataSource
extension MasterServiceViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return services.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueCell(cls: ServiceTableViewCell.self, indexPath: indexPath) else {
            return UITableViewCell(style: .default, reuseIdentifier: "")
        }
        cell.cellSource(services[indexPath.row])
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            showLoader()
            ClientAPI.shared.deleteMasterService(services[indexPath.row], completionHandler: { (success, error) in
                self.hideLoader()
                if let error = error {
                    return self.error(error.localizedDescription)
                }
                self.services.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: .automatic)
            })
        }
    }
}

// MARK: - UITableViewDelegate
extension MasterServiceViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: Segue.EditServiceSegue.rawValue, sender: tableView)
    }
}

// MARK: - AddServiceDelegate
extension MasterServiceViewController: AddServiceDelegate {
    func serviceChanged() {
        loadServices()
    }
}
