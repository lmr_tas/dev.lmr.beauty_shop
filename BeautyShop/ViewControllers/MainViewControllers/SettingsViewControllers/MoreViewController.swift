//
//  MoreViewController.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 18.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit
import Parse

class MoreViewController: BaseViewController {

    @IBOutlet weak var userTextView: UITextView!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var profileButton: UIButton!
    @IBOutlet weak var radiusSlider: UISlider!
    
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        updateUI()
    }

    // MARK: - IBActions
    @IBAction func loginAction(_ sender: Any) {
        switchTo(.LoginNavigationController)
    }
    
    @IBAction func profileAction(_ sender: Any) {
        switchTo(.ProfileNavigationController)
    }
    
    @IBAction func sliderAction(_ sender: Any) {
        print(radiusSlider.value.rounded())
        User.current()?.radius = Double(radiusSlider.value.rounded())
    }
    
    @IBAction func addServices(_ sender: Any) {
        RandomService().createRandomService { (success, error) in
            if let error = error {
                return self.error(error.localizedDescription)
            }
            self.error("Success!!!")
        }
    }
    

    @IBAction func logoutAction(_ sender: Any) {
        showLoader()
        User.logOutInBackground { (error) in
            self.hideLoader()
            if let error = error {
                return self.error(error.localizedDescription)
            }
            self.switchTo(.MainTabBarViewController)
        }
    }
    
    // MARK: - Private
    private func updateUI() {
        let isUser = User.current() != nil
        userTextView.text = User.current().debugDescription
         //print("[updateUI]", User.current()?.categories as Any)
        loginButton.isEnabled   = !isUser
        logoutButton.isEnabled  = isUser
        profileButton.isEnabled = isUser
    }
}
