//
//  BaseViewController.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 15.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit
import SVProgressHUD

class BaseViewController: UIViewController {

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Conrollers switcher
    func switchTo(_ controller: Controllers, with animation: UIViewAnimationOptions = .transitionCrossDissolve) {
        RootVCSwitcher().switchToRootController(controller, with: animation)
    }
    
    // MARK: - Progress HUD
    func showLoader(mask: SVProgressHUDMaskType = .none) {
        UIApplication.showHUD(withMask: mask)
    }
    
    func hideLoader() {
        UIApplication.hideHUD()
    }
    
    // MARK: - Error message
    func error(_ message: String) {
        UIApplication.showAlertMessage(message)
    }
    
}
