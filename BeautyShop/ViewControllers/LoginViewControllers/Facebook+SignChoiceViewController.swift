//
//  SignChoiceViewController.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 14.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit

class SignChoiceViewController: BaseViewController {

    //MARK: - Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }
    
    //MARK: - IBActions
    @IBAction func cancelButtonAction(_ sender: Any) {
        self.switchTo(.MainTabBarViewController)
    }
    
    @IBAction func FBLoginButtonAction(_ sender: Any) {
        ClientAPI.shared.loginViaFacebook { (user, error, isNew) in
            self.hideLoader()
            if let error = error {
                return self.error(error.localizedDescription)
            }
            if isNew {
                self.switchTo(.ProfileNavigationController)
            } else {
                self.switchTo(.MainTabBarViewController)
            }
        }
    }

}
