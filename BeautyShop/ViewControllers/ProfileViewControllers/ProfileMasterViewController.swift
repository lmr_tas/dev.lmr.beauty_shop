//
//  MasterProfileViewController.swift
//  TASBeautyShop
//
//  Created by Alexey Trotsenko on 10/4/17.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit
import CoreLocation
import Parse

class MasterProfileViewController: BaseViewController {

    @IBOutlet var addressLabel: UILabel!
    @IBOutlet var tableView: UITableView!

    let mapSegue = "ProfileMapViewController"
    let user = User.current()
    var userAvatar: UIImage?
    var categories = [Category]()
    
    let locationManager = LocationManager()
    var address: String? {
        willSet {
            addressLabel.text = newValue
        }
    }
    var coordinate: CLLocationCoordinate2D?

    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        address = user?.address
        coordinate = user?.coordinate
        getCategory()
    }

    // MARK: - IBAction
    @IBAction func checkAddressAction(_ sender: Any) {
        locationManager.delegate = self
        if locationManager.getLocation() {
            self.showLoader()
        }
    }
    
    @IBAction func editAddressAction(_ sender: Any) {
        UIApplication.showAlertWithTextField(title: "Edit address:", message: address, fieldText: address,
                                             actionTitle: "Done") { (newAddress) in
                                                self.address = newAddress
        }
    }
    
    @IBAction func saveButtonAction(_ sender: Any) {
        // Check address
        guard let streetAddress = addressLabel.text else {
            return error("Enter correct address or enable location")
        }
        self.coordinateFrom(streetAddress, complition: {
            self.updateUserGeo()
            // Check selected category
            guard let categories = self.tableView.selectedItems(self.categories) else {
                return self.error("Plese select categories")
            }
            self.user?.categories = categories
            ClientAPI.shared.updateUserProfile(with: self.userAvatar, completionHandler: { (isSuccess, error) in
                self.hideLoader()
                if let error = error {
                    return self.error(error.localizedDescription)
                }
                self.switchTo(.MainTabBarViewController)
            })
            
        }, failure: { (error) in
            return self.error(error)
        })
    }

    // MARK: - Private
    private func getCategory() {
        self.showLoader()
        ClientAPI.shared.loadCategories { (categories, error) in
            self.hideLoader()
            if let error = error {
                return self.error(error.localizedDescription)
            }
            guard let categories = categories else {
                return self.error("Get category from server error")
            }
            self.categories = categories
            self.tableView.reload()
            self.selectCategory()
        }
    }

    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == mapSegue {
            let controller = segue.destination as! ProfileMapViewController
            controller.coordinates = self.coordinate
        }
    }
}

// MARK: - UITableViewDataSource
extension MasterProfileViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return categories.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CategoryCell", for: indexPath)
        let category = categories[indexPath.row]
        cell.textLabel?.text = category.name
        return cell
    }
    
    private func selectCategory() {
        guard let userCategories = user?.categories else { return }
        for (idx, category) in categories.enumerated() {
            for userCategory in userCategories {
                if category.objectId!.isEqual(userCategory.objectId) {
                    DispatchQueue.main.async {
                        self.tableView.selectRow(at: IndexPath(row: idx, section: 0),
                                                 animated: true, scrollPosition: .bottom)
                    }
                }
            }
        }
    }
}

// MARK: - LocationDelegate
extension MasterProfileViewController: LocationDelegate {
    func locationUpdated(_ locationManager: LocationManager, coordinate: CLLocationCoordinate2D?, error: Error?) {
        self.hideLoader()
        if let error = error {
            return self.error(error.localizedDescription)
        }
        let address = locationManager.currentAddress
        self.address = address
        self.coordinate = coordinate
    }
    
    func coordinateFrom(_ address: String, complition: @escaping ()->(), failure: @escaping FailureErrorBlock) {
        locationManager.getLocationFrom(address) { (coordinate, error) in
            if let _ = error {
                return failure(LocationErorr.getEnteredAddress.localizedDescription)
            }
            self.address = address
            self.coordinate = coordinate
            return complition()
        }
    }
    
    private func updateUserGeo() {
        user?.address = address
        user?.location = PFGeoPoint(latitude: coordinate!.latitude,
                                    longitude: coordinate!.longitude)
    }
}

