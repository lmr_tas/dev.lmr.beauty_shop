//
//  ProfileViewController.swift
//  TASBeautyShop
//
//  Created by Alexey Trotsenko on 9/27/17.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController {
    
    @IBOutlet var saveButton: UIButton!
    @IBOutlet var avatarImageView: UIImageView!
    @IBOutlet var nameTextField: UITextField!
    @IBOutlet var telTextField: UITextField!
    @IBOutlet var roleSegmentControl: UISegmentedControl!
    
    let segueToMaster = "MasterProfileViewController"
    
    var isImageEditing = false
    var currentUser = User.current()
    lazy var imagePicker = ImagePickerManager()
    
    var userType: User.UserType = .customer
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        print(self.navigationController as Any)
        title = "User profile"
        updateUserUI()
    }
    
    // MARK: - IBAction
    @IBAction func selectUserTypeAction(_ sender: Any) {
        //TODO: Test for stability
        userType = User.UserType(rawValue: roleSegmentControl.selectedSegmentIndex)!
        setTitleConfirmButton(userType)
    }
    
    @IBAction func userAvaButtonAction(_ sender: UIButton) {
        imagePicker.delegate = self
        imagePicker.presentImagePicker()
    }
    
    @IBAction func cancelAction(_ sender: Any) {
        self.switchTo(.MainTabBarViewController)
    }
    
    @IBAction func saveButtonAction(_ sender: UIButton) {
        if let userName = currentUser?.name, userName.isEmpty {
            self.error("Enter correct name")
            return
        }
        if currentUser?.services != nil && userType == .customer {
            return error("First delete all service")
        }
        currentUser?.userTypeValue = userType
        currentUser?.radius = 50

        switch userType {
        case .master:
            self.performSegue(withIdentifier: segueToMaster, sender: nil)
        case .customer:
            self.showLoader()
            ClientAPI.shared.updateUserProfile(with: isImageEditing ? avatarImageView?.image : nil, completionHandler: { (isSuccess, error) in
                self.hideLoader()
                if let error = error {
                    return self.error(error.localizedDescription)
                }
                self.switchTo(.MainTabBarViewController)
            })
        }
    }
    
    // MARK: - Private
    private func updateUserUI() {
        if let userType = currentUser?.userTypeValue {
            self.userType = userType
        }
        roleSegmentControl.selectedSegmentIndex = userType.rawValue
        setTitleConfirmButton(userType)

        nameTextField.text = currentUser?.name
        avatarImageView?.imageFromPFFile(file: currentUser?.avatar)
        telTextField.text = "1234567890"
    }
    
    private func setTitleConfirmButton(_ type: User.UserType) {
        saveButton?.setTitle(type == .customer ? "Save":"Next", for: .normal)
    }
    
    // MARK: - Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == segueToMaster {
            let controller = segue.destination as! MasterProfileViewController
            controller.userAvatar = isImageEditing ? avatarImageView?.image : nil
        }
    }
}

extension ProfileViewController: ImagePickerManagerDelegate {
    func pickedImage(_ image: UIImage?) {
        isImageEditing = true
        avatarImageView?.image = image
    }
}

extension ProfileViewController: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let name = textField.text, !name.isEmpty  {
            currentUser?.name = name
        }
    }
}
