//
//  ProfileMapViewController.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 26.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit

class ProfileMapViewController: BaseViewController {
    
    @IBOutlet var mapView: MKMapView!
    var coordinates: CLLocationCoordinate2D?
    var annotation: ProfileMapAnnotation?
    
    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if coordinates != nil {
            annotation = ProfileMapAnnotation(coordinates!)
            self.mapView.addAnnotation(annotation!)
        } else {
            mapView.showsUserLocation = true
        }
    }
}

extension ProfileMapAnnotation: MKMapViewDelegate {
    
    /*- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
     {
     if ([annotation isKindOfClass:[MKUserLocation class]])
     return nil;
     
     static NSString *reuseId = @"pin";
     MKPinAnnotationView *pav = (MKPinAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:reuseId];
     if (pav == nil)
     {
     pav = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:reuseId];
     pav.draggable = YES;
     pav.canShowCallout = YES;
     }
     else
     {
     pav.annotation = annotation;
     }
     
     return pav;
     }*/
    /*
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView? {
        
        if annotation.isKind(of: MKUserLocation.self) {
            return nil
        }
        let reuseId = "annotationPin"
        var pin = mapView.dequeueReusableAnnotationView(withIdentifier: reuseId, for: annotation)
        if pin == nil {
            pin = MKAnnotationView(annotation: annotation, reuseIdentifier: reuseId)
            pin.isDraggable = true
            pin.canShowCallout = true
        } else {
            pin.annotation = annotation
        }
        return pin
    }
    */
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, didChange newState: MKAnnotationViewDragState, fromOldState oldState: MKAnnotationViewDragState) {
        
    }
}
