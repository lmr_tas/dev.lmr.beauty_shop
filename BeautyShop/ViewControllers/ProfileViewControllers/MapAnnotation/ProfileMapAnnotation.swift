//
//  ProfileMapAnnotation.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 26.12.2017.
//  Copyright © 2017 Alexey Trotsenko. All rights reserved.
//

import UIKit
import MapKit

class ProfileMapAnnotation: NSObject, MKAnnotation {
    
    var coordinate: CLLocationCoordinate2D
    
    init(_ coordinate: CLLocationCoordinate2D) {
        self.coordinate = coordinate
    }
    
}
