//
//  RandomService.swift
//  BeautyShop
//
//  Created by Alexey Trotsenko on 19.01.2018.
//  Copyright © 2018 Alexey Trotsenko. All rights reserved.
//

import UIKit
import Parse

class RandomService: NSObject {

    func createRandomService(completionHandler: @escaping (Bool, Error?) -> ()) {
        
        ClientAPI.shared.loadCategories { (category, error) in
            if let error = error {
                return completionHandler(false, error)
            }
            if let categories = category {
                let cat = categories[Int(arc4random() % UInt32(categories.count))]
                self.createService(on: cat, completionHandler: { (success, error) in
                    completionHandler(success, error)
                })

            }
        }
    }
    
    private func createService(on category: Category, completionHandler: @escaping (Bool, Error?)->()) {
        let categoryID = category.objectId
        let image = UIImage(named: categoryID!)
        let service = Service(name: category.name + " \(self.stringWithLength())", price: Double(arc4random() % 1000),
                              duration: Int(arc4random() % 60), description: "\(self.stringWithLength(len: 100))", category: category)
        service.location = PFGeoPoint(latitude: self.nearestRandomPoint(47.8504849),
                                      longitude: self.nearestRandomPoint(35.1148157))
        ClientAPI.shared.updateService(service, image: image, completionHandler: { (success, error) in
            completionHandler(success, error)
        })
        
    }
    
    private func stringWithLength (len : Int = 3) -> String {
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789 "
        let randomString : NSMutableString = NSMutableString(capacity: len)
        for _ in 0..<len {
            let length = UInt32 (letters.length)
            let rand = arc4random_uniform(length)
            randomString.appendFormat("%C", letters.character(at: Int(rand)))
        }
        return randomString as String
    }
    
    private func nearestRandomPoint(_ coordinate: Double) -> Double {
        var c = coordinate
        let iteration = arc4random() % 15
        for _ in 0..<iteration {
            let randomDouble = Double(arc4random() % 1000)/10000
            let direction = arc4random() % 2
            if direction == 0 {
                c += randomDouble
            } else {
                c -= randomDouble
            }
        }
        return c
    }
}
